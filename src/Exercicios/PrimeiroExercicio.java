package Exercicios;

import java.util.Scanner;

public class PrimeiroExercicio {

	public static void main(String[] args) {
// 		TODO USAR CAMMEL CASE PARA MÉTODOS E VARIÁVEIS
//		1a LETRA minúscula depois 1a letra da próxima palavra maiúcula: exe: idadeAnos

/**
 * Entrada do USUÁRIO:
 * 3 anos 2 meses e 15 dias
 */
//		int IdadeAnos, IdadeMeses, IdadeDias, IdadeTotalDias;

//		System.out.print("Idade em dias\n");

//		System.out.print("Digite os anos\n");
//		IdadeAnos = entrada.nextInt();
//
//		System.out.print("Digite os meses\n");
//		IdadeMeses = entrada.nextInt();
//
//		System.out.print("Digite os dias\n");
//		IdadeDias = entrada.nextInt();

//		IdadeAnos = getEntrada("Digite os anos");
//		IdadeMeses = getEntrada("Digite os meses");
//		IdadeDias = getEntrada("Digite os dias");
//
////
//
//		int gaba = -1;
//		gaba = getEntrada("Deseja fazer uma soma? (1 para sim)");

//		if (gaba == 1) calculaSoma();
	}

//	public static void calculaSoma() {
//		int a,b;
//		a = getEntrada("Digite o 1o valor");
//		b = getEntrada("Digite o 2o valor");
//
//		System.out.println("Sua soma de a + b = " + (a + b));
//	}

	public static void calculaDias() {
		int IdadeAnos, IdadeMeses, IdadeDias, IdadeTotalDias;

		IdadeAnos = getEntrada("Digite os anos");

		IdadeMeses = getEntrada("Digite os meses");

		IdadeDias = getEntrada("Digite os dias");


		IdadeTotalDias = IdadeAnos * 365 + IdadeMeses * 30 + IdadeDias;
		System.out.print("Idade total em dias = " + IdadeTotalDias + ")\n\n" );
	}

	public static int getEntrada(String message) {
		int retorno = -1;

		do {
			Scanner entrada = new Scanner(System.in);
			System.out.print(message + "\n");
			try {
				retorno = entrada.nextInt();
			} catch (Exception e) {
				System.out.println("Entrada inválida, digite apenas números");
			}

		} while (retorno == -1);

		return retorno;
	}

}